CONFIG_DIR := ~/.config
NVIM_DIR := $(CONFIG_DIR)/nvim

include bash/Makefile
include git/Makefile
include nvim/Makefile
include tmux/Makefile
include xkb/Makefile
include zsh/Makefile
include mac/Makefile

.PHONY: version
version:
	@echo 2020.0209.0

